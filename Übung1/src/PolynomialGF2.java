import java.util.Arrays;

public class PolynomialGF2 {

    static final boolean[] zeroAr= null;
    static final boolean[] oneAr= {true};
    static final PolynomialGF2 zero=new PolynomialGF2(zeroAr);
    static final PolynomialGF2 one=new PolynomialGF2();
    boolean[] koefs;


    private PolynomialGF2(){                                //default konst
        koefs=oneAr;
    }

    private PolynomialGF2(boolean[] ko){                    //param: booleanfeld, wird bei führenden false getrimmt bis pos0 == true
        koefs=trim(ko);

    }

    public boolean[] toArray(){                             //gibt kopie des arrays zurück

        boolean[] boBack = new boolean[koefs.length];
        for(int i = 0; i< koefs.length; i++){
            boBack[i]=koefs[i];
        }
        return boBack;
    }



    public boolean isZero(){
        return this.equals(zero);

    }

    public boolean isOne(){
        return this.equals(one);

    }

    private static boolean[] trim(boolean[] in) {                   //zählt wie viel false bis true kommen, kürzt array um diese Anzahl
        if(in==null){
            return in;
        }
        if(in[0]){
            return in;
        }
        if(!in[0] && in.length==1){
            return zeroAr;
        }
        else {
            int i = 1;
            while (!in[i] && i< in.length-1) {
                i++;
            }
            //System.out.println(i);
            if(i==in.length){
                //System.out.println("ReachedNull");
                return zeroAr;
            }
            else {
                boolean[] out = new boolean[in.length - i];
                for (int b = 0; b < in.length - i; b++) {
                    out[b] = in[b + i];
                   // System.out.println(out[b]);
                }
                if(!out[0] && out.length==1){
                    return zeroAr;
                }

                return out;
            }
        }
    }

    public PolynomialGF2 clone(){                                       //kopiert Array und gibt neues Polynom mit dem neuen Array zurück

        boolean[] copy = new boolean[koefs.length];
        for(int i=0; i< koefs.length; i++){
            copy[i]=koefs[i];
        }
        return new PolynomialGF2(copy);
    }

    public boolean equals(PolynomialGF2 comp){                          //Vergleich
        if(comp==null){
            return false;
        }
        if(this == comp){
            return true;
        }
        return Arrays.equals(this.koefs, comp.koefs);
    }

    public int hashCode(){
        int h=0;
        for(int i = koefs.length-1; i>=0; i--){
           if(koefs[i]){
               h=h+(int)Math.pow(2, koefs.length-1-i);
           }
        }
        return h;
    }

    @Override
    public String toString() {
        if(this.isZero()){
            return "null";
        }
        int pos;
        String s="";
        for(int i =0; i<this.koefs.length; i++){ //Wenn letzter Koeffizient erreicht wurde, wird kein *x^ mehr gebraucht
            if(i== koefs.length-1){
                if(koefs[i]){
                    s=s+"1";
                }
                else{
                    s=s+"0";
                }
            }
            else if(koefs[i]){
                pos=koefs.length-i-1;
                s=s+"1"+"x^"+pos+" "+"+"+" ";
            }
        }
        return s;
    }

    public PolynomialGF2 plus(PolynomialGF2 in){            //Plusfunktion im Endeffekt XOR

        if(this.isZero() && !in.isZero()){
            return in;
        }

        if(in.isZero() && !this.isZero()){
            return this;
        }


        if(in.koefs.length> koefs.length){                  //Aufpassen, dass die richtigen Stellen miteinander verglichen werden
           PolynomialGF2 out = in.clone();
           int dif = in.koefs.length - koefs.length;

            for(int i= koefs.length-1; i>=0; i--){

                out.koefs[i+dif]=koefs[i] ^ in.koefs[i+dif];

            }
            return out;
        }
        else{
            PolynomialGF2 out = this.clone();
            int dif = koefs.length - in.koefs.length;

            for(int i= in.koefs.length-1; i>=0; i--){

                out.koefs[i+dif]=in.koefs[i] ^ koefs[i+dif];
            }
            return out;
        }
    }

    public PolynomialGF2 times(PolynomialGF2 in){
        boolean[] outAr = new boolean[koefs.length+in.koefs.length-1];//
        for(int i=in.koefs.length-1; i>=0; i--){                                        //Multiplikant von rechts nach links durchgehen, bei 1 multiplizieren
            int shift = in.koefs.length-i-1;


            if(in.koefs[i]) {                                                           //Wenn Multiplikant true ist, muss man nur noch an richtiger Stelle zum Ergebnis addieren
                for (int b = koefs.length - 1; b >= 0; b--) {
                    outAr[outAr.length - shift - (koefs.length-b)] = koefs[b] ^ outAr[outAr.length - shift - (koefs.length-b)];
                }
            }
        }
        return new PolynomialGF2(outAr);
    }

    public int degree(){
        return koefs.length-1;
    }

    public PolynomialGF2 shift(int k){
        boolean[] multiAr = {true, false};
        PolynomialGF2 multi = new PolynomialGF2(multiAr);
        PolynomialGF2 out = this.clone();
        for(int i = 0; i<k; i++){
            out=out.times(multi);

        }
    return out;

    }

    public static PolynomialGF2 modTwo(PolynomialGF2 first, PolynomialGF2 second){

        if(first == null || second == null ){
            System.out.println("Polynom darf nicht null sein");

        }
        if(second.isZero()){
            System.out.println("nicht durch null teilbar");
        }
        PolynomialGF2 divid = first.clone();
        PolynomialGF2 out = first.clone();
        for(int i = 0; i<out.koefs.length; i++){
            out.koefs[i]=false;
        }

        while(divid.degree() > second.degree()){                                        //Wenn Divident größer als Divisor ist, solange bis Divid gleich/kleiner
            PolynomialGF2 divis = second.shift(divid.degree()- second.degree());
            PolynomialGF2 res = divid.plus(divis);
            divid.koefs = res.toArray();
            divid.koefs=trim(divid.koefs);
        }

        if(divid.degree() == second.degree()){
            out=divid.plus(second);
        }
        else{
            out.koefs=divid.toArray();

        }
        return out;
    }


    public PolynomialGF2 pow(int k){                                                    //Für Test2, potenziert
        boolean[] standAr={true, true};
        PolynomialGF2 stand = new PolynomialGF2(standAr);
        PolynomialGF2 out = this.clone();
        if(k==0){
            return one;
        }
        for(int i = 0; i<k-1; i++){
            out=out.times(stand);
        }
        return out;
    }


    public static void main(String[] args) {

        boolean[] v3Ar = {true, false, false, false, true, true, false, true, true};
        boolean[] v4Ar = {true, false, true, true};

        PolynomialGF2 v3 = new PolynomialGF2(v3Ar);
        PolynomialGF2 v4 = new PolynomialGF2(v4Ar);

        System.out.println("Test 1:");
        for(int i = 0; i<7; i++){
            System.out.print(i + " ");
            System.out.print(modTwo(one.shift(i), v4).hashCode() + " ");
            System.out.print(modTwo(one.shift(i), v4));
            System.out.println();
        }

        System.out.println();
        System.out.println("Test 2:");
        boolean[] stanAr = {true, true};
        PolynomialGF2 stan = new PolynomialGF2(stanAr);

        System.out.println("\t"+ "  0  1   2   3   4   5   6   7   8   9   a   b   c   d   e   f");
        System.out.println("\t"+ "______________________________________________________________");
        for(int i = 0; i<16; i++) {

            System.out.printf("%x | ", i);

            for (int j = 0; j < 16; j++) {
                System.out.printf(" %02x ", modTwo(stan.pow(i * 16 + j), v3).hashCode());
            }
            System.out.println();
        }
    }
}



