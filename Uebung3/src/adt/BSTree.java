package adt;

import visualtree.Knoten;


import java.util.Collection;
import java.util.Iterator;
import java.util.function.IntFunction;
import java.util.Stack;

/**
 * BSTKlasse, implementiert einen binaeren Suchbaum
 * @param <E> Typ des Suchbaums
 */
public class BSTree<E extends Comparable<E>> extends java.util.AbstractCollection<E>{


    /**
     * Groesseneigenschaft des Baums, Anzahl der Knoten
     */
    private int size;
    /**
     * Wurzel des Baums, keine uebergeordneten Knoten
     */
    private Knoten<E> root = new Knoten<>(null, null, null);

    /**
     * Default Constructor
     */
    public BSTree(){
        size=0;
    }

    /**
     * Parametrisierter Constructor, der Wert der Wurzel wird hier gesetzt
     * @param value Wert fuer root
     */
    public BSTree(E value){
        //Knoten<E> root = new Knoten<>(value, null, null);
        root.setVal(value);
        size=1;
    }


    /**
     * Getter fuer den Wurzelknoten
     * @return Wurzelknoten
     */
    public Knoten<E> getRoot(){
        return this.root;
    }


    /**
     * Getter fuer BSTgroesse
     * @return BSTgroesse
     */
    @Override
    public int size() {
        return size;
    }

    @Override
    public <T> T[] toArray(IntFunction<T[]> generator) {

        return null;
    }

    /**
     * In-order Sortierung der Werte des Baums in Arrayform
     * @return inorder-Array
     */
    public Object[] toArray(){
        Object[] out = new Object[size];
        Stack<Knoten<?>> stack = new Stack<>();
        if(size==0){
            return out;
        }
        Knoten<?> current = this.root;
        int i =0;
        while(current!=null || !stack.isEmpty()){       //Linken Knoten auf Stack pushen wenn vorhanden
            while(current!=null){
                stack.push(current);
                current=current.getL();
            }
            current=stack.pop();                        //Knoten aus Stack, Elternknoten, current auf rechten Knoten setzen
            out[i]=current.getValue();
            i++;
            current=current.getR();


        }
        return out;
    }

    /**
     * Liefert passenden Iterator zurueck
     * @return Iterator
     */
    @Override
    public Iterator<E> iterator() {
        return new BSTreeIterator<>(this);
    }

    /**
     *Ueberprueft ob Baum leer ist
     * @return true, wenn Baumgroesse gleich 0, sonst false
     */
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * Sucht nach Wert im BST
     * @param o der zu suchende Wert
     * @return true, wenn Wert gefunden, sonst false
     */
    public boolean contains(Object o){
        if(size==0){
            System.out.println("kein tree vorhanden");
        }


        Knoten<E> current = root;

       try {                                                            //Try, weil Object nicht unbedingt Baumtyp entspricht
           while (true) {                                               //Einfach mit BSTStruktur Baum durchsuchen
               if (current.getValue().compareTo((E) o) == 0) {
                   return true;

               } else if (current.getValue().compareTo((E) o) < 0) {
                   if (current.getR() == null) {
                       return false;
                   }
                   current = current.getR();
               } else {
                   if (current.getL() == null) {
                       return false;
                   }
                   current = current.getL();
               }
           }
       }
       catch(ClassCastException e){
           System.out.println();
           System.out.println();
           e.printStackTrace();
           return false;
       }



    }

    /**
     * Ruft fuer jeden Wert der Collection die Containsmethode auf
     * @param c die zu durchsuchende Collection
     * @return true, wenn jeder Wert der Collection im Baum enthalten ist, sonst false
     */
    public boolean containsAll(Collection<?> c){

        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Liefert Stringrepraesentation vom Baum inorder zurueck
     * @return Baum als String
     */
    public String toString(){
        return "<" +toString(root) + ">";
    }


    /**
     * Helfermethode fuer toString, arbeitet rekursiv
     * @param r Knoten, von dem der Wert und weitere Knoten abgelesen werden
     * @return Knotenwerte als String
     */
    public static String toString(Knoten<?> r){
        if(r==null)
            return "";
        else
            return toString(r.getL()) + " " +r.getValue() + " " +toString(r.getR());
    }

    /**
     * Fuegt ein Wert als Knoten in den Baum ein
     * @param e der einzufuegende Wert
     * @return true, wenn einfuegen stattgefunden hat
     */
    public boolean addE(E e){
       Knoten<E> toAdd = new Knoten<>(e, null, null); //Aus E e einen passenden Knoten erstellen
       Knoten<E> current = root;
       //wenn size == 0 dann ist toAdd=root;

        if(size==0){
            root=toAdd;
            return true;
        }
       while(true){                                             //BST durchgehen und Knoten toAdd an passender Stelle einfügen
           if(current.getValue().compareTo(e)>0){
               if(current.getL()==null){
                   current.setL(toAdd);
                   size++;
                   break;
               }
               else{
                   current=current.getL();
               }
           }
           else{
               if(current.getR()==null){
                   current.setR(toAdd);
                   size++;
                   break;
               }
               else{
                   current=current.getR();
               }
           }
       }
       size=size++;                                             //nach erfolgreichem Einfügen wird der Baum um einen Knoten größer
        return true;
    }

    /**
     * Methode um Wert aus Baum zu entfernen
     * @param o Zu entfernender Wert
     * @return true, wenn entfernen stattgefunden hat
     */
    public boolean remove(Object o){
        if(!this.contains(o)) {
            System.out.println("Element nicht in Baum enthalten");
            return false;
        }
        root=removeRec(root, (E) o);                            //ruft Hilfsmethode auf
        return true;
    }

    /**
     * Rekursiv arbeitende Methode, da je nach Knoten die Entferung etwas anders ablaeuft
     * Unterscheidung ob Knoten kein, ein oder zwei Kinder besitzt
     * @param root zu pruefender Knoten
     * @param key Wert des Knotens
     * @return naechster Knoten
     */
    public Knoten<E> removeRec(Knoten<E> root, E key){
        if(root==null){
            return root; //wird "eingesetzt" wenn löschen stattfindet
        }

        //Werte entlang des Baumes vergleichen
        if(key.compareTo(root.getValue())<0){
            root.setL(removeRec(root.getL(), key));
        }
        else if(key.compareTo(root.getValue())>0){
            root.setR(removeRec(root.getR(), key));
        }

        else{
            //sollte Knoten nur ein oder kein weiteren Knoten haben:
            if(root.getL()==null){
                return root.getR();
            }
            else if(root.getR()==null){
                return root.getL();
            }

            //sollte Knoten zwei weitere Knoten haben:
            //nächsten in-order Knotenwert kopieren
            root.setVal(min(root.getR()));

            //nächsten in-order Knoten dann löschen
            root.setR(removeRec(root.getR(), root.getValue()));


        }
        return root;

    }

    /**
     * Liefert minimalsten Wert vom Subbaum, beginnend vom uebergebenem Knoten
     * @param out Knoten, ab dem gesucht wird
     * @return min Wert
     */
    public E min(Knoten<E> out){
        E minval = out.getValue();
        while(out.getL()!=null){

            minval=out.getL().getValue();
            out=out.getL();
        }
        return minval;
    }

    /**
     * Minvalue vom ganzen Baum
     * @return min Wert
     */
    public E minVal(){
        Knoten<E> current = root;
        if(size==0) {
            return null;
        }
        while(current.getL()!=null){
            current=current.getL();
        }
        return current.getValue();
    }

    /**
     * Maxvalue vom ganzen Baum
     * @return max Wert
     */
    public E maxVal(){
        Knoten<E> current = root;
        if(size==0) {
            return null;
        }
        while(current.getR()!=null){
            current=current.getR();
        }
        return current.getValue();
    }




}
