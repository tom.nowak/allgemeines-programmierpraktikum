package algebra;

/**
 * Klasse um Brueche zu vergleichen
 */
public class CompRational extends Rational implements java.lang.Comparable<CompRational>{


    /**
     * Zaehler
     */
    private long numerator;
    /**
     * Nenner
     */
    private long denumerator;


    /**
     * Vergleicht zwei Brueche
     * @param o mit diesem Bruch wird verglichen
     * @return 1 wenn aufrufender Bruch groesser, 0 wenn gleich, negativ wenn kleiner ist
     */
    @Override
    public int compareTo(CompRational o) {

        if(this.numerator*o.denumerator>this.denumerator*o.numerator){
            return 1;
        }
        else if(this.numerator*o.denumerator<this.denumerator*o.numerator){
            return -1;
        }
        else{

            return 0;
        }
    }

    /**
     * Vergleichendes Bruchobjekt
     * @param num Zaehler
     * @param denum Nenner
     */
    public CompRational(long num, long denum){
        super(num, denum);
        numerator=num;
        denumerator=denum;

    }

    /**
     * Ausfuehrbare Klasse um Bruchvergleiche zu testen
     * @param args
     */
    public static void main(String[] args){

        long drei = 3;
        long vier =4;
        long fuenf=5;
        long acht =8;

        CompRational one = new CompRational(drei, vier);
        CompRational two = new CompRational(fuenf, acht);
        System.out.println(one.compareTo(two));
    }

    /**
     * Macht aus einer double einen Bruch, annaehernd jedenfalls
     * @param in die umzuwandelnde double
     * @return Bruch
     */
    public CompRational toComp(double in){
        int i=0;
        while(in%1!=0){
            in=in*10;
            i++;
        }
        long outNu = (long) in;
        long outDe = (long) Math.pow(10, i);
        return new CompRational(outNu, outDe);

    }

    /**
     * Aus Bruch zu double
     * @return double
     */
    public double toDouble(){
        return (double) numerator/denumerator;
    }





}
