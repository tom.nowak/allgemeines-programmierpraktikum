package visualtree;

/**
 * Knotenklasse, aus diesen wird der BST gebaut
 * @param <T> Knotenwerttyp
 */
public class Knoten<T> implements DrawableTreeElement<T> {


    /**
     * Wert des Knotens
     */
    private T val;
    /**
     * Verweis auf rechten Knoten
     */
    private Knoten<T> r;
    /**
     * Verweis auf linken Knoten
     */
    private Knoten<T> l;

    /**
     * Parametrisierter Constructor
     * @param value Wert des Knotens
     * @param right Verweis auf rechten Knoten
     * @param left Verweis auf linken Knoten
     */
    public Knoten(T value, Knoten<T> right, Knoten<T> left) {
        val = value;
        r=right;
        l=left;
    }

    /**
     * Default Contructor
     */
    public Knoten(){
        val=null;
        l=null;
        r=null;
    }

    /**
     * Setter fuer Knotenwert
     * @param val Knotenwert
     */
    public void setVal(T val) {
        this.val = val;
    }

    /**
     * Setter fuer rechten Knoten
     * @param r Verweis
     */
    public void setR(Knoten<T> r) {
        this.r = r;
    }

    /**
     * Setter fuer linken Knoten
     * @param l Verweis
     */
    public void setL(Knoten<T> l) {
        this.l = l;
    }

    @Override
    public DrawableTreeElement<T> getLeft() {
        return l;
    }

    /**
     * Getter vom rechten Knoten
     * @return Verweis
     */
    public Knoten<T> getR() {
        return r;
    }

    /**
     * Getter vom linken Knoten
     * @return Verweis
     */
    public Knoten<T> getL() {
        return l;
    }

    @Override
    public DrawableTreeElement<T> getRight() {
        return r;
    }

    @Override
    public boolean isRed() {
        return false;
    }

    /**
     * Getter fuer Knotenwert
     * @return Knotenwert
     */
    @Override
    public T getValue() {
        return val;
    }





}
