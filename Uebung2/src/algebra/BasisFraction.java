package algebra;

import static algebra.Rational.euc;

/**
 * BasisFraction implementiert die arithmetischen Methoden fuer Brueche
 */
public abstract class BasisFraction implements Fractional {

    /**
     * Methodenvorlage um Zaehler und Nenner zu setzen
     * @param numerator Zaehler
     * @param denumerator Nenner
     */
    protected abstract void setND(long numerator, long denumerator);


    /**
     * Addiert operand auf aufrufendes Objekt
     * @param operand
     */
    @Override
    public void add(Fractional operand) {

        this.setND(this.getN()* operand.getD()+this.getD()* operand.getN(), this.getD()* operand.getD());
        long ggT=euc(this.getN(), this.getD());

        if(ggT<0){
            ggT=ggT*(-1);
        }
        this.setND(this.getN()/ggT, this.getD()/ggT);
    }


    /**
     * Subtrahiert operand vom aufrufenden Objekt
     * das wird durch Addition der Negation des operanden erreicht
     * @param operand
     */
    @Override
    public void sub(Fractional operand) {
        this.add(operand.negation());
        long ggT=euc(this.getN(), this.getD());
        operand.negation();

        if(ggT<0){
            ggT=ggT*(-1);
        }
        this.setND(this.getN()/ggT, this.getD()/ggT);

    }

    /**
     * Multipliziert operand mit aufrufendem Objekt
     * @param operand
     */
    @Override
    public void mul(Fractional operand) {
        this.setND(this.getN()* operand.getN(), this.getD()* operand.getD());
        long ggT=euc(this.getN(), this.getD());

        if(ggT<0){
            ggT=ggT*(-1);
        }
        this.setND(this.getN()/ggT, this.getD()/ggT);


    }

    /**
     * Dividiert Objekt mit operand, das wird durch
     * multiplikation mit Umkehrwert erreicht
     * @param operand
     */
    @Override
    public void div(Fractional operand) {
        this.mul(operand.reciprocal());
        long ggT=euc(this.getN(), this.getD());
        operand.reciprocal();

        if(ggT<0){
            ggT=ggT*(-1);
        }
        this.setND(this.getN()/ggT, this.getD()/ggT);


    }

    /**
     * negiert aufrufendes Objekt
     * @return negation
     */
    @Override
    public Fractional negation() {

        this.setND(-1*this.getN(), this.getD());
        return this;
    }

    /**
     * Bildet den Umkehrwert vom aufrufenden Objekt
     * d.h. es tauscht Zaehler/Nenner
     * @return Umkehrwert
     */
    @Override
    public Fractional reciprocal() {

        long n=this.getN();
        this.setND(this.getD(), n);
        if(this.getD()==0){
            System.out.println("Nenner darf nicht 0 sein");
        }
        if(this.getD()<0){
            this.setND(this.getN()*(-1), this.getD()*(-1));
        }
        return this;
    }




}
