package algebra;

/**
 * Interface, gibt arithmetische und getter Methoden vor
 */
public interface Fractional {
    /**
     * @return Zaehler
     */
    // get numerator
    long getN();

    /**
     * @return Nenner
     */
    // get denominator
    long getD();

    /**
     * Addition
     * @param operand
     */
    // add operand to object
    void add(Fractional operand);

    /**
     * Subtraktion
     * @param operand
     */
    // subtract operand from object
    void sub(Fractional operand);

    /**
     * Multiplikation
     * @param operand
     */
    // multiply object by operand
    void mul(Fractional operand);

    /**
     * Division
     * @param operand
     */
    // divide object by operand
    void div(Fractional operand);

    /**
     * Negation
     * @return
     */
    // new additive inverse object
    Fractional negation ();

    /**
     * Umkehrwert
     * @return
     */
    // new multiplicative inverse object
    Fractional reciprocal ();
}
