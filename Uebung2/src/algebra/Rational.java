package algebra;

import java.util.Objects;

/**
 * Rational repraesentiert einen Bruch, also Zaehler und Nenner.
 * Der Nenner ist immer positiv, der Bruch teilerfremd.
 */
public class Rational extends BasisFraction {


    /**
     * Zaehler
     */
    private long numerator;
    /**
     * Nenner
     */
    private long denumerator;

    /**
     * Setzt Zaehler und Nenner
     * @param numerator Zaehler
     * @param denumerator Nenner
     */
    @Override
    protected void setND(long numerator, long denumerator) { //Hier die Kürzenmethode!
        this.numerator = numerator;
        this.denumerator = denumerator;
    }

    /**
     * Liefert Zaehler
     * @return numerator
     */
    @Override
    public long getN() {
        return numerator;
    }

    /**
     * Liefert Nenner
     * @return denumerator
     */
    @Override
    public long getD() {
        return denumerator;
    }


    /**
     * Rationalconstructor, teilerfremd, Nenner darf nicht 0 sein
     * @param numerator Nenner
     * @param denumerator Zaehler
     */
    public Rational(long num, long denum){

        long ggT=euc(num, denum);

        if(ggT<0){
            ggT=ggT*(-1);
        }


        if(denum==0){
            System.out.println("Nenner darf nicht 0 sein");
        }
        if(denum<0){
            num=num*(-1);
            denum=denum*(-1);
        }

        numerator=num/ggT;
        denumerator=denum/ggT;


    }

    /**
     * Euklid Algo. um groessten gemeinsamen Teiler von a und b zu finden
     * @param a
     * @param b
     * @return gcd
     */
    public static long euc(long a, long b){
        long c;
        while(b!=0){
             c=a%b;
             a=b;
             b=c;
        }
        return a;
    }

    /**
     * @return Stringrepraesentation
     */
    @Override
    public String toString() {


        if(this.getD()==1){
            return numerator + "";
        }

        return numerator + "/" + denumerator;
    }

    /**
     * Vergleicht zwei Objekte
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rational rational = (Rational) o;
        return numerator == rational.numerator && denumerator == rational.denumerator;
    }

    /**
     * Fuegt Zaehler und Nenner zusammen
     * @return hashCode
     */
    @Override
    public int hashCode() {


        String s1 = Long.toString(numerator);
        String s2 = Long.toString(denumerator);


        String s = s1 + s2;


        int c = Integer.parseInt(s);
        return c;
    }


    /**
     * Erstellt ein neuen Bruch mit gleichem Zahler/Nenner
     * @return cloned object
     */
    public Rational clone(){
        return new Rational(this.getN(), this.getD());
    }

}
