package testing;

import algebra.Fractional;
import algebra.Rational;

import java.util.Stack;

/**
 * Klasse um Ausdruecke in RPN auszuwerten
 */
public class RPN {


    /**
     * Ausfuehrbare Testmethode die Ausdruecke in RPN durch args bekommt und auswertet.
     * Dies wird mit Stacks realisiert.
     * @param args Ausdruck in RPN
     */
    public static void main (String[] args){




            Stack<Fractional> stRPN = new Stack();
            for(int i=0; i< args.length; i++){
                try {
                switch (args[i]) {
                    case "+" -> {
                        Fractional one = stRPN.pop();
                        stRPN.peek().add(one);
                    }
                    case "-" -> {
                        Fractional two = stRPN.pop();
                        stRPN.peek().sub(two);
                    }
                    case "*" -> {
                        Fractional three = stRPN.pop();
                        stRPN.peek().mul(three);
                    }
                    case "/" -> {
                        Fractional four = stRPN.pop();
                        stRPN.peek().div(four);
                    }

                    default -> {
                        try {
                            stRPN.push(new Rational(Long.parseUnsignedLong(args[i]), 1));

                        } catch (NumberFormatException e) {
                            System.out.println("Falsches Zeichen: " + args[i]);
                            return;
                        }
                    }
                }
            }
            catch (Exception e){
                System.out.println("Ungültiger Rechenausdruck");
                return;
            }
            }

            if(stRPN.size()!=1){
                System.out.println("Ungültiger Rechenausdruck");
                return;
            }


            System.out.println(stRPN.pop());






    }








}
