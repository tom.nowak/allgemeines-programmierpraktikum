package testing;

import adt.BSTree;

import adt.BSTreeIterator;
import algebra.CompRational;
import java.util.Random;



/**
 * Ausfuehrbare Klasse um die Funktionalitaet von der Baumklasse zu testen
 */
public class BSTreeTester {

    public static void main(String[] args){


        if(args.length%2!=0){
            System.out.println("Ungerade Anzahl von Zahlen");
            return;
        }

        CompRational rootComp = new CompRational(Long.parseLong(args[0]),Long.parseLong(args[1]));

        BSTree<CompRational> compTree = new BSTree<>(rootComp);

        for(int i=2; i< args.length; i+=2){
            compTree.addE(new CompRational(Long.parseLong(args[i]),Long.parseLong(args[i+1])));
        }
        //System.out.println(compTree);
        System.out.println();

        BSTreeIterator<CompRational> itComp = new BSTreeIterator<>(compTree);
        System.out.println("Primärer Baum:");
        while(itComp.hasNext()){
            System.out.println(itComp.next());
        }
        System.out.println();

        BSTree<CompRational> compTreeSecOne = new BSTree<>(new CompRational(Long.parseLong(args[0]), Long.parseLong(args[1])));
        BSTree<CompRational> compTreeSecTwo = new BSTree<>(new CompRational(Long.parseLong(args[2]), Long.parseLong(args[3])));
        for(int i = 4; i< args.length; i+=4){

            compTreeSecOne.addE(new CompRational(Long.parseLong(args[i]),Long.parseLong(args[i+1])));
            compTreeSecTwo.addE(new CompRational(Long.parseLong(args[i+2]),Long.parseLong(args[i+3])));

        }
        BSTreeIterator<CompRational> itCompOne = new BSTreeIterator<>(compTreeSecOne);
        BSTreeIterator<CompRational> itCompTwo = new BSTreeIterator<>(compTreeSecTwo);
        System.out.println("erster sekundärer Baum:");
        while(itCompOne.hasNext()){
            System.out.println(itCompOne.next());
        }
        System.out.println();
        System.out.println("zweiter sekundärer Baum:");
        while(itCompTwo.hasNext()){
            System.out.println(itCompTwo.next());
        }
        System.out.println();

        System.out.println("Ist der erste sekundäre Baum im primären Baum enthalten?");
        System.out.println(compTree.containsAll(compTreeSecOne));
        System.out.println("Ist der zweite sekundäre Baum im primären Baum enthalten?");
        System.out.println(compTree.containsAll(compTreeSecOne));
        System.out.println();

        System.out.println("Ist die erste übergebene Zahl: " + args[0] + "/" + args[1] + " im primären Baum enthalten?");
        System.out.println(compTree.contains(rootComp));
        System.out.println("Ist die letzte übergebene Zahl: " + args[args.length-2] + "/" + args[args.length-1] + " im primären Baum enthalten?");
        CompRational lastComp = new CompRational(Long.parseLong(args[args.length-2]), Long.parseLong(args[args.length-1]));
        System.out.println(compTree.contains(lastComp));
        System.out.println();


        System.out.println("Baum bevor Löschung von: " + rootComp);
        System.out.println(compTree);
        compTree.remove(rootComp);

        System.out.println("Baum bevor Löschung von: " + lastComp);
        System.out.println(compTree);

        compTree.remove(lastComp);
        System.out.println("Baum nach Löschung von: " + lastComp);
        System.out.println(compTree);
        System.out.println();

        Random randGen = new Random();


        double treeMin=compTree.minVal().toDouble();
        double treeMax=compTree.maxVal().toDouble();


        for(int i=0; i<200; i+=2){
            double rand = randGen.nextDouble()*(treeMax-treeMin)+treeMin;
            CompRational compRand=new CompRational(1, 1);
            rand=round(rand, 2);

            compRand=compRand.toComp(rand);

            //System.out.println("Liegt " + compRand + " im Baum?");
            if(compTree.contains(compRand)){
                System.out.println(compRand + " liegt im Baum");
                compTree.remove(compRand);
                System.out.println(compRand + " wurde entfernt, Baum nach Entfernung:");
                System.out.println(compTree);

            }


        }

    }

    /**
     * Rundet Doublewerte.
     * @param value die zu rundende Zahl
     * @param places Stellen auf die gerundet werden soll, min: 0
     * @return gibt gerundete Double zurueck
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }



}
