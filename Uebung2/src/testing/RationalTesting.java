package testing;

import algebra.Rational;

/**
 * Testklasse fuer Bruchrechnung
 */
public class RationalTesting extends Rational {


    /**
     * Rationalconstructor, teilerfremd, Nenner darf nicht 0 sein
     *
     * @param num Zaehler
     * @param denum Nenner
     */
    public RationalTesting(long num, long denum) {
        super(num, denum);
    }

    /**
     * Ausfuehrbare Methode um die Rechnungen zu testen
     * @param args
     */
    public static void main(String[] args){

        long zwei =2;
        long drei=3;
        long vier=4;
        long fuenf=5;

        Rational dreivier = new Rational(drei, vier);
        Rational dreifuenf = new Rational(drei, fuenf);


        System.out.println(dreivier +" plus");
        System.out.println(dreifuenf + " ergibt");
        dreivier.add(dreifuenf);
        System.out.println(" "+ dreivier);
        System.out.println();


        System.out.println(dreivier +" minus");
        System.out.println(dreifuenf + " ergibt");
        dreivier.sub(dreifuenf);
        System.out.println(dreivier);
        System.out.println();


        System.out.println(dreifuenf +" mal");
        System.out.println(dreifuenf + " ergibt");
        dreifuenf.mul(dreifuenf);
        System.out.println(dreifuenf);
        System.out.println();


        System.out.println(dreivier +" geteilt durch");
        System.out.println(dreifuenf + " ergibt");
        dreivier.div(dreifuenf);
        System.out.println(dreivier);
        System.out.println();

        Rational zweidrei = new Rational(zwei, drei);
        Rational vierfuenf = new Rational(vier, fuenf);


        System.out.println(zweidrei +" plus");
        System.out.println(vierfuenf + " ergibt");
        zweidrei.add(vierfuenf);
        System.out.println(" "+ zweidrei);
        System.out.println();


        System.out.println(zweidrei +" minus");
        System.out.println(vierfuenf + " ergibt");
        zweidrei.sub(vierfuenf);
        System.out.println(zweidrei);
        System.out.println();


        System.out.println(vierfuenf +" mal");
        System.out.println(vierfuenf + " ergibt");
        vierfuenf.mul(vierfuenf);
        System.out.println(vierfuenf);
        System.out.println();


        System.out.println(zweidrei +" geteilt durch");
        System.out.println(vierfuenf + " ergibt");
        zweidrei.div(vierfuenf);
        System.out.println(zweidrei);
        System.out.println();








        System.out.println("-25/-100 wird zu:");
        Rational twFvHun = new Rational(-25, -100);
        System.out.println(twFvHun);
        System.out.println();

        System.out.println("der Hashcode von 1/4 ist:");
        System.out.println(twFvHun.hashCode());
        System.out.println();

        System.out.println("der Hashcode von -1/4 ist:");
        Rational onefour = new Rational(-1, 4);
        System.out.println(onefour.hashCode());
        System.out.println();


        System.out.println("Ist 1/4 == -1/4?:");
        System.out.println(onefour.equals(twFvHun));
        System.out.println();

        System.out.println("Ist 1/4 geklont == 1/4?");
        Rational onefourClone = onefour.clone();
        System.out.println(onefour.equals(onefourClone));
        System.out.println();


        System.out.println("der Hashcode von 5/-1 ist:");
        Rational five = new Rational(5, -1);
        System.out.println(five.hashCode() + "hash");
        System.out.println();


        System.out.println("Ist -5/1 == 1/41?:");
        System.out.println(onefour.equals(twFvHun));
        System.out.println();

        System.out.println("Ist -5/1 geklont == -5/1?");
        Rational fiveClone = five.clone();
        System.out.println(fiveClone.equals(five));
        System.out.println();








    }



}
