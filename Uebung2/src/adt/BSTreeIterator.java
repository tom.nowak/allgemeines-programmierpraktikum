package adt;

import visualtree.Knoten;

import java.util.Iterator;
import java.util.Stack;

/**
 * Iteratorklasse fuer BST
 * @param <E> Iteratortyp
 */
public class BSTreeIterator<E extends Comparable<E>> implements Iterator<E> {

    /**
     * Uebergebener Tree fuer den Iterator
     */
    private BSTree<E> tree;
    /**
     * aktueller Knoten fuer Iteration
     */
    private Knoten<E> current;
    /**
     * Knotenstack, benoetigt fuer die Iteration
     */
    private Stack<Knoten> st;


    public BSTreeIterator(BSTree<E> tree){
        st = new Stack<>();
        this.tree=tree;
        current=tree.getRoot();
        while(current!=null){
            st.push(current);
            current=current.getL();
        }

    }


    /**
     * Ueberprueft, ob noch ein Eintrag folgt
     * @return true, wenn noch nicht fertig iteriert.
     */
    @Override
    public boolean hasNext() {
        return !st.isEmpty();
    }

    /**
     * Liefert nächsten Knotenwert zurück
     * @return nächsten Knotenwert
     */
    @Override
    public E next() {       //In-order: left root right

        Knoten<E> node = st.pop();
        Knoten<E> temp = node.getR();

        while(temp!=null){
            st.push(temp);
            temp=temp.getL();
        }
        return node.getValue();

    }
}
