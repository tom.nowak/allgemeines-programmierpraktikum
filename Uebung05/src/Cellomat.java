import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Eine Klasse die grundlegende Eigenschaften von frame einstellt
 */
class AppFrame extends JFrame {
    public AppFrame(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}


/**
 * Klasse die JPanel um benoetigte Funktionen erweitert
 */
class paintPanel extends JPanel{
    /**
     * Die "Mal"-Methode
     * @param g benötigter Parameter
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(int x = 0; x<512; x++){
            for(int y = 0; y<512; y++){
                if(Cellomat.cellCalc[x][y]){
                    g.setColor(Color.BLACK);

                }
                else{
                    g.setColor(Color.WHITE);
                }
                g.fillRect(x, y, 1, 1);
            }
        }


    }

}

/**
 * Threadklasse, errechnet und erstellt den naechsten Zustand
 */
class cellCalc extends Thread{


    /**
     * Die treibende Methode des Threads, hier wird gerechnet
     */
    synchronized public void run() {

        while(true){
            try {
                wait();

                JPanel futpat = new paintPanel();
                futpat.setBackground(Color.WHITE);

                Cellomat.frame.add(futpat);
                Boolean[][] future = new Boolean[512][512];
                for (int i = 0; i < Cellomat.rounds; i++) {
                    for (int x = 0; x < 512; x++) {
                        for (int y = 0; y < 512; y++) {
                            future[x][y] = Cellomat.isNeighbourOdd(Cellomat.cellCalc, x, y);
                        }
                    }
                    for (int x = 0; x < 512; x++) {
                        for (int y = 0; y < 512; y++) {
                            Cellomat.cellCalc[x][y] = future[x][y];
                        }
                    }
                    futpat.revalidate();
                    Cellomat.frame.revalidate();
                    Cellomat.frame.repaint();
                }
                futpat.revalidate();



            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }


    /**
     * Notifymethode fuer den Thread
     */
    synchronized public void sendNotify(){
        notify();
    }




}

/**
 * Enthaelt ausfuehrbare Mainmethode, hier befindet sich der gesamte Code
 */
public class Cellomat
{

    /**
     * Array was den Automaten darstellt, initialisiert mit Bild, aktualisiert durch Thread
     */
    public static Boolean[][] cellCalc = new Boolean[512][512];
    /**
     * Repraesentiert die angegebene Anzahl von Berechnungen/Runden
     */
    public static int rounds;
    /**
     * Der Frame auf dem der Automat angezeigt wird
     */
    public static JFrame frame = new AppFrame("Allgemeines Programmierpraktikum");
    /**
     * Ausfuehrbare Methode um das Programm auszufuehren
     * @param args Bilddatei
     */
    public static void main( String[] args )
    {
        cellCalc calc = new cellCalc();
        calc.start();

        final BufferedImage image;                                                          //Bild einlesen
        try {
            image = ImageIO.read(new File(args[0]));

        }
        catch (IOException e){
            System.out.println("Bild nicht gefunden");
            System.exit(0);
            return;
        }






        //Startarray, was mit Bild initialisiert wrird
        Boolean[][] cells = new Boolean[512][512];
        for(int x = 0; x<512; x++){
            for(int y = 0; y<512; y++){
                cells[x][y]=false;
            }
        }
        for(int i=0; i<image.getWidth(); i++){              //i=breite
            for(int b=0; b<image.getHeight(); b++){         //b=höhe

                Color is = new Color(image.getRGB(i, b));
                int grayI =(int) (is.getRed()+is.getBlue()+is.getGreen())/3;

                int startOffset = 256-32;
                if(grayI/52.0==4){
                }

                else if(grayI/52==3){
                    cells[startOffset+i*2][startOffset+b*2]=true;

                }
                else if(grayI/52==2){
                    cells[startOffset+i*2][startOffset+b*2]=true;
                    cells[startOffset+i*2+1][startOffset+b*2+1]=true;
                }
                else if(grayI/52==1){
                    cells[startOffset+i*2+1][startOffset+b*2]=true;
                    cells[startOffset+i*2][startOffset+b*2+1]=true;
                    cells[startOffset+i*2+1][startOffset+b*2+1]=true;
                }
                else if(grayI/52==0){
                    cells[startOffset+i*2][startOffset+b*2]=true;
                    cells[startOffset+i*2+1][startOffset+b*2]=true;
                    cells[startOffset+i*2][startOffset+b*2+1]=true;
                    cells[startOffset+i*2+1][startOffset+b*2+1]=true;

                }
            }
        }



        //Startpanel
        JPanel pat = new JPanel(){                                                          //Pattern Panel
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                for(int i=0; i<image.getWidth(); i++){              //i=breite
                    for(int b=0; b<image.getHeight(); b++){         //b=höhe

                        Color is = new Color(image.getRGB(i, b));
                        int grayI =(int) (is.getRed()+is.getBlue()+is.getGreen())/3;
                        int startOffset = 256-32;

                        if(grayI/52.0==4){
                            g.setColor(Color.white);
                            g.fillRect(startOffset+i*2, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2, startOffset+b*2+1, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2+1, 1, 1);
                        }
                        else if(grayI/52==3){
                            g.setColor(Color.black);
                            g.fillRect(startOffset+i*2, startOffset+b*2, 1, 1);
                            g.setColor(Color.white);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2, startOffset+b*2+1, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2+1, 1, 1);
                        }
                        else if(grayI/52==2){
                            g.setColor(Color.black);
                            g.fillRect(startOffset+i*2, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2+1, 1, 1);
                            g.setColor(Color.white);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2, startOffset+b*2+1, 1, 1);
                        }
                        else if(grayI/52==1){
                            g.setColor(Color.white);
                            g.fillRect(startOffset+i*2, startOffset+b*2, 1, 1);
                            g.setColor(Color.black);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2, startOffset+b*2+1, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2+1, 1, 1);
                        }
                        else if(grayI/52==0){
                            g.setColor(Color.black);
                            g.fillRect(startOffset+i*2, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2, 1, 1);
                            g.fillRect(startOffset+i*2, startOffset+b*2+1, 1, 1);
                            g.fillRect(startOffset+i*2+1, startOffset+b*2+1, 1, 1);
                        }
                    }
                }
            }
        };
        pat.setLayout(new GridLayout(512, 512, 0,0 ));
        pat.setBackground(Color.WHITE);


        //Panel für Knopf und Textfeld
        JPanel panelN = new JPanel();



        //Panel mit Boxlayout erstellen, um Glue einzufügen, dahinter dann Knopf
        JPanel panelS = new JPanel();
        panelS.setLayout(new BoxLayout(panelS, BoxLayout.LINE_AXIS));
        panelS.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panelS.add(Box.createHorizontalGlue());

        //Alles in Frame packen
        frame.add(panelN, BorderLayout.NORTH);
        frame.add(pat, BorderLayout.CENTER);
        frame.add(panelS, BorderLayout.SOUTH);








        //Buttons erstellen
        JButton b3 = new JButton("Go");
        JButton bClose = new JButton("Close");
        JTextField input = new JTextField(4);



        //Buttons hinzufügen
        panelS.add(bClose);
        panelN.add(b3, BorderLayout.EAST);
        panelN.add(input, BorderLayout.WEST);




        //Jeweils die Actions für die Buttons erstellen
        b3.addActionListener(new ActionListener() {
            @Override
            synchronized public void actionPerformed(ActionEvent e) {


                try {
                    rounds = Integer.parseInt(input.getText());
                }
                catch(NumberFormatException b){
                    System.out.println("Enter Integer");
                }
                for (int x = 0; x < 512; x++) {
                    for (int y = 0; y < 512; y++) {
                        cellCalc[x][y] = cells[x][y];
                    }
                }
                frame.remove(pat);
                calc.sendNotify();      //Hier findet dann der Call zum Thread statt
            }
        });


        bClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);
            }
        });


        //Finale Einstellungen
        Dimension sz = new Dimension(512, 620);
        frame.setPreferredSize(sz);
        frame.pack();
        frame.setVisible(true);



    }

    //Es folgen die einzelnen Methoden für die Nachbarabfragen


    public static boolean getRightNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX== nb.length-1){
            return nb[0][posY];
        }
        else{
            return nb[posX+1][posY];
        }
    }

    public static boolean getLeftNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX==0){
            return nb[nb.length-1][posY];
        }
        else{
            return nb[posX-1][posY];
        }
    }

    public static boolean getTopNeighbour(Boolean[][] nb, int posX, int posY){
        if(posY==0){
            return nb[posX][nb[posX].length-1];
        }
        else{
            return nb[posX][posY-1];
        }
    }

    public static boolean getBottomNeighbour(Boolean[][] nb, int posX, int posY){
        if(posY==nb[posX].length-1){
            return nb[posX][0];
        }
        else{
            return nb[posX][posY+1];
        }
    }

    public static boolean getTopLeftNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX==0){
            if(posY==0){
                return nb[nb.length-1][nb[posX].length-1];                      //oben links wird zu unten rechts
            }

            else {                                                              //erste Spalte
                return nb[nb.length-1][posY-1];
            }
        }
        else if(posY==0){                                                       //Obere Reihe
            return nb[posX-1][nb[posX].length-1];
        }
        else {                                                                  //Standard
            return nb[posX-1][posY-1];
        }
    }

    public static boolean getTopRightNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX==nb.length-1){
            if(posY==0){
                return nb[0][nb[posX].length-1];                                //Oben rechts wird zu unten links
            }
            else{                                                               //Letzte Spalte
                return nb[0][posY-1];
            }
        }
        else if(posY==0){                                                       //Obere Reihe
            return nb[posX+1][nb[posX].length-1];
        }
        else{
            return nb[posX+1][posY-1];                                          //Standard
        }
    }

    public static boolean getBottomLeftNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX==0){
            if(posY==nb[posX].length-1){
                return nb[nb.length-1][0];                                      //Unten links wird zu oben rechts
            }
            else{                                                               //erste Spalte
                return nb[nb.length-1][posY+1];
            }
        }
        else if(posY==nb[posX].length-1){                                       //Unterste Reihe
            return nb[posX-1][0];
        }
        else{                                                                   //Standard
            return nb[posX-1][posY+1];
        }
    }

    public static boolean getBottomRightNeighbour(Boolean[][] nb, int posX, int posY){
        if(posX==nb.length-1){
            if(posY==nb[posX].length-1){
                return nb[0][0];                                                //Unten rechts wird zu oben links
            }
            else{                                                               //Letzte Spalte
                return nb[0][posY+1];
            }
        }
        else if(posY==nb[posX].length-1){                                       //Unterste Reihe
            return nb[posX+1][0];
        }
        else{                                                                   //Standard
            return nb[posX+1][posY+1];
        }
    }

    /**
     * Methode, die alle Nachbarschaftsabfragen zusammenfasst
     * @param in das Pixelgrid
     * @param posX Position X
     * @param posY Position Y
     * @return True wenn ungerade Anzahl lebender Nachbarn
     */
    //Zusammenfassende Methode für alle Nachbarabfragen
    public static boolean isNeighbourOdd(Boolean[][] in, int posX, int posY){
        int nb=2;
        if(getRightNeighbour(in, posX, posY)){
            nb++;
        }
        if(getLeftNeighbour(in, posX, posY)){
            nb++;
        }
        if(getTopNeighbour(in, posX, posY)){
            nb++;
        }
        if(getBottomNeighbour(in, posX, posY)){
            nb++;
        }
        if(getTopLeftNeighbour(in, posX, posY)){
            nb++;
        }
        if(getTopRightNeighbour(in, posX, posY)){
            nb++;
        }
        if(getBottomRightNeighbour(in, posX, posY)){
            nb++;
        }
        if(getBottomLeftNeighbour(in, posX, posY)){
            nb++;
        }
        return nb % 2 != 0;
    }

}
