import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Eine Klasse die grundlegende Eigenschaften von frame einstellt
 */
class AppFrame extends JFrame {
    public AppFrame(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

/**
 * Enthaelt ausfuehrbare Mainmethode, hier befindet sich der gesamte Code
 */
public class AppDrawEvent
{
    /**
     * Ausfuehrbare Methode um das Programm auszufuehren
     * @param args Bilddatei
     */
    public static void main( String[] args )
    {

        final BufferedImage image;                                                          //Bild einlesen
        try {
            image = ImageIO.read(new File(args[0]));

        }
        catch (IOException e){
            System.out.println("Bild nicht gefunden");
            System.exit(0);
            return;
        }



        JPanel ori = new JPanel() {                                                         //original Panel
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                for(int i=0; i<image.getWidth(); i++){              //i=breite
                    for(int b=0; b<image.getHeight(); b++){         //b=höhe

                        Color is = new Color(image.getRGB(i, b));
                        g.setColor(is);

                        g.fillRect(20+i*2, 20+b*2, 2, 2);

                    }
                }
            }
        };

        JPanel gray = new JPanel(){                                                         //Gray Panel
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                for(int i=0; i<image.getWidth(); i++){              //i=breite
                    for(int b=0; b<image.getHeight(); b++){         //b=höhe

                        Color is = new Color(image.getRGB(i, b));
                        int grayI =(int) (is.getRed()+is.getBlue()+is.getGreen())/3;
                        Color gray = new Color(grayI, grayI, grayI);
                        g.setColor(gray);

                        g.fillRect(20+i*2, 20+b*2, 2, 2);

                    }
                }
            }
        };

        JPanel pat = new JPanel(){                                                          //Pattern Panel
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                for(int i=0; i<image.getWidth(); i++){              //i=breite
                    for(int b=0; b<image.getHeight(); b++){         //b=höhe

                        Color is = new Color(image.getRGB(i, b));
                        int grayI =(int) (is.getRed()+is.getBlue()+is.getGreen())/3;


                        if(grayI/52.0==4){
                            g.setColor(Color.white);
                            g.fillRect(20+i*2, 20+b*2, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2, 1, 1);
                            g.fillRect(20+i*2, 20+b*2+1, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2+1, 1, 1);

                        }

                        else if(grayI/52==3){
                            g.setColor(Color.black);
                            g.fillRect(20+i*2, 20+b*2, 1, 1);
                            g.setColor(Color.white);
                            g.fillRect(20+i*2+1, 20+b*2, 1, 1);
                            g.fillRect(20+i*2, 20+b*2+1, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2+1, 1, 1);



                        }
                        else if(grayI/52==2){
                            g.setColor(Color.black);
                            g.fillRect(20+i*2, 20+b*2, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2+1, 1, 1);
                            g.setColor(Color.white);
                            g.fillRect(20+i*2+1, 20+b*2, 1, 1);
                            g.fillRect(20+i*2, 20+b*2+1, 1, 1);




                        }
                        else if(grayI/52==1){
                            g.setColor(Color.white);
                            g.fillRect(20+i*2, 20+b*2, 1, 1);
                            g.setColor(Color.black);
                            g.fillRect(20+i*2+1, 20+b*2, 1, 1);
                            g.fillRect(20+i*2, 20+b*2+1, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2+1, 1, 1);


                        }

                        else if(grayI/52==0){
                            g.setColor(Color.black);
                            g.fillRect(20+i*2, 20+b*2, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2, 1, 1);
                            g.fillRect(20+i*2, 20+b*2+1, 1, 1);
                            g.fillRect(20+i*2+1, 20+b*2+1, 1, 1);

                        }




                    }
                }
            }
        };




        JFrame frame = new AppFrame("Allgemeines Programmierpraktikum");
        JPanel panel = new JPanel();


        JPanel panelN = new JPanel();



        //Panel mit Boxlayout erstellen, um Glue einzufügen, dahinter dann Knopf
        JPanel panelS = new JPanel();
        panelS.setLayout(new BoxLayout(panelS, BoxLayout.LINE_AXIS));
        panelS.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panelS.add(Box.createHorizontalGlue());

        //Alles in Frame packen
        frame.add(panelN, BorderLayout.NORTH);
        frame.add(panel, BorderLayout.CENTER);
        frame.add(panelS, BorderLayout.SOUTH);
        frame.add(ori);







        //Buttons erstellen
        JButton b1 = new JButton("Original");  //Buttons erstellen
        JButton b2 = new JButton("Grayscale");
        JButton b3 = new JButton("Pattern");
        JButton bClose = new JButton("Close");


        b1.setEnabled(false);

        panelS.add(bClose);
        panelN.add(b1,  BorderLayout.WEST);                     //Buttons zum oberen Panel hinzufügen
        panelN.add(b2, BorderLayout.CENTER);
        panelN.add(b3, BorderLayout.EAST);

        //Jeweils die drei Actions für die Buttons erstellen
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b2.setEnabled(true);
                b1.setEnabled(false);
                b3.setEnabled(true);

                frame.add(ori);
                frame.remove(gray);
                frame.remove(pat);
                frame.revalidate();
                frame.repaint();
            }
        });




        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b1.setEnabled(true);
                b2.setEnabled(false);
                b3.setEnabled(true);
                frame.remove(ori);
                frame.remove(pat);
                frame.add(gray);
                frame.revalidate();
                frame.repaint();

            }
        });



        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b1.setEnabled(true);
                b2.setEnabled(true);
                b3.setEnabled(false);
                frame.remove(ori);
                frame.remove(gray);
                frame.add(pat);
                frame.revalidate();
                frame.repaint();
            }
        });


        bClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


        //Finale Einstellungen
        Dimension sz = new Dimension(440, 525);
        frame.setPreferredSize(sz);
        frame.pack();
        frame.setVisible(true);


    }





}
